<?php

declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: guillaumeloulier
 * Date: 23/07/2018
 * Time: 11:16
 */

namespace App\UI\Action\Interfaces;


use App\UI\Responder\Interfaces\HomeResponderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface HomeActionInterface
{
    /**
     * @param Request $request
     * @param HomeResponderInterface $responder
     * @return Response
     */
    public function __invoke(
        Request $request,
        HomeResponderInterface $responder
    ): Response;
}
